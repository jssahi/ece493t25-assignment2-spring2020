import numpy as np
import pandas as pd


class rlalgorithm:
    def __init__(self, actions, *args, **kwargs):
        learning_rate=0.01
        reward_decay=0.9
        e_greedy=0.1
        
        self.actions = actions  
        self.lr = learning_rate
        self.gamma = reward_decay
        self.epsilon = e_greedy
        self.q_table = pd.DataFrame(columns=self.actions, dtype=np.float64)  
        self.display_name="Expected Sarsa"

    def choose_action(self, observation):
        self.check_state_exist(observation)
 
        if np.random.uniform() >= self.epsilon:
           
            state_action = self.q_table.loc[observation, :]
           
            action = np.random.choice(state_action[state_action == np.max(state_action)].index)
        else:
         
            action = np.random.choice(self.actions)
        return action

    def learn(self, s, a, r, s_):
        self.check_state_exist(s_)

        if s_ != 'terminal':
            state_action_values = self.q_table.loc[s_,:]
            value_sum = np.sum(state_action_values)
            max_value = np.max(state_action_values)
            max_value_count = len(state_action_values[state_action_values == max_value])

            # get probability for greedy
            prob_max = 1 - self.epsilon 

            # calculate expected value for greedy
            expected_value_max = max_value * prob_max 

            # calculate probability for non greedy
            prob_non_max = self.epsilon 
            
            # calculate expected value for non greedy
            expected_value_non_max = (value_sum - max_value * max_value_count) * prob_non_max

            # expected value is the summation of the two
            expected_value = expected_value_max + expected_value_non_max

            q_target = self.gamma * expected_value 
        else:
            q_target = 0

        # Q(s, a) = Q(s, a) + α(r + γ Σ_a(π(a, s_)Q(s_, a)) - Q(s, a)) 
        self.q_table.loc[s, a] = self.q_table.loc[s, a] + self.lr * (r + q_target - self.q_table.loc[s, a])

        return s_, self.choose_action(str(s_))

    '''States are dynamically added to the Q(S,A) table as they are encountered'''
    def check_state_exist(self, state):
        if state not in self.q_table.index:
            # append new state to q table
            self.q_table = self.q_table.append(
                pd.Series(
                    [0]*len(self.actions),
                    index=self.q_table.columns,
                    name=state,
                )
            )