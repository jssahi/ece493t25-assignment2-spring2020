import numpy as np
import pandas as pd

import random

class rlalgorithm:
    def __init__(self, actions, *args, **kwargs):
        """Your code goes here""" 
        learning_rate=0.01
        reward_decay=0.9
        e_greedy=0.1
        
        self.actions = actions  
        self.lr = learning_rate
        self.gamma = reward_decay
        self.epsilon = e_greedy
        self.q_table = pd.DataFrame(columns=self.actions, dtype=np.float64)  
        self.q_table2 = pd.DataFrame(columns=self.actions, dtype=np.float64)  
        self.display_name="Double QLearning"

    def choose_action(self, observation):
        self.check_state_exist(observation)
 
        if np.random.uniform() >= self.epsilon:
           
            state_action = self.q_table.loc[observation, :]  + self.q_table2.loc[observation, :]
           
            action = np.random.choice(state_action[state_action == np.max(state_action)].index)
        else:
         
            action = np.random.choice(self.actions)
        return action


    '''Update the Q(S,A) state-action value table using double q learning '''
    def learn(self, s, a, r, s_):
        """Your code goes here"""
        self.check_state_exist(s_) 

        if np.random.uniform() >= 0.5:
           
            if s_ != 'terminal':
                q_max = self.q_table2.loc[s_, :].max()
            else:
                q_max = 0

            # Q1(s, a) = Q1(s, a) + α(r + γmaxQ2(s_, a) - Q1(s, a)) 
            self.q_table.loc[s, a] = self.q_table.loc[s, a] + self.lr * (r + self.gamma * q_max - self.q_table.loc[s, a])
        else:
            if s_ != 'terminal':
                q_max = self.q_table.loc[s_, :].max()
            else:
                q_max = 0

            # Q2(s, a) = Q2(s, a) + α(r + γmaxQ1(s_, a) - Q2(s, a)) 
            self.q_table2.loc[s, a] = self.q_table2.loc[s, a] + (self.lr * (r + (self.gamma * q_max) - self.q_table2.loc[s, a]))
        
        
        return s_, self.choose_action(str(s_))

    '''States are dynamically added to the Q(S,A) table as they are encountered'''
    def check_state_exist(self, state):
        if state not in self. q_table.index:
            # append new state to q table
            self. q_table = self. q_table.append(
                pd.Series(
                    [0]*len(self.actions),
                    index=self. q_table.columns,
                    name=state,
                )
            )

        if state not in self. q_table2.index:
            # append new state to q table
            self. q_table2 = self. q_table2.append(
                pd.Series(
                    [0]*len(self.actions),
                    index=self. q_table2.columns,
                    name=state,
                )
            )